// --------------------------------
// projects/c++/collatz/Collatz.c++
// Copyright (C) 2018
// Glenn P. Downing
// --------------------------------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair

#include "Collatz.h"

using namespace std;


// ------------
// cache
// ------------

/**
 * int size is the length of the cache
 * long[] cache is a lazy cache that stores cycle lengths
 */
static int size = 1000000;
long* cache = new long[size];

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// collatz_length
// ------------

int collatz_length (long long n) {
    if (n < size && cache[n] > 0) {
        return  cache[n];
    }

    if (n == 1) {
        return 1;
    }
    else if (n == 2) {
        return 2;
    }
    else if (n % 2 == 0) {
        long long ret = 1 + collatz_length(n / 2);

        if (n < size && cache[n] <= 0) {
            cache[n] = ret;
        }

        return ret;
    }
    else {
        long long ret = 1 + collatz_length(3 * n + 1);

        if (n < size && cache[n] <= 0) {
            cache[n] = ret;
        }

        return ret;
    }
}

// ------------
// collatz_eval
// ------------

int collatz_eval (int i, int j) {
    assert(i >= 0);
    assert(j >= 0);
    assert(i <= 1000000);
    assert(j <= 1000000);

    int maxLength = 0;
    //When the start point is greater than the end point we must switch
    //the two values
    int temp = 0;
    if (i > j) {
        temp = j;
        j = i;
        i = temp;
    }

    //If half of the end point plus one is greater than the start point
    //then we can start there and skip the lower half of the interval
    int m = (j / 2) + 1;
    if (i < m) {
        i = m;
    }

    for (int k = i; k <= j; ++k) {
        int length = collatz_length(k);
        if (length > maxLength) {
            maxLength = length;
        }
    }

    return  maxLength;
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    string s;
    while (getline(r, s)) {
        const pair<int, int> p = collatz_read(s);
        const int            i = p.first;
        const int            j = p.second;
        const int            v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}